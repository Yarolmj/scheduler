# The Scheduler
The basic C program that attempts to emulate the internal dynamics of some process scheduling algorithms including:
- First Come First Served (FCFS)
- Shortest job first (SJF)
- Round Robin (RR)
- Priority Scheduling (PS)
- Priority Scheduling with Round Robin (PS_RR)
- Multilevel Queue Scheduling (MQS)
- Multilevel Feedback Queue Scheduling (MFQS)
## Compiling
```bash
make build
```
## Running with default example
```bash
make run
```
## Usage
You can load one of the custom examples for each algoritm via:
```bash
./Scheduler examples/ALGORITM.txt
```
or you can write your how configuration creating a text file with the following format:
```
algoritm=ID OF THE ALGORITM YOU WANT TO RUN (see first section)
expropiative_mode=0 for non preemptive mode or 1 for preemptive mode
process_count=number of processes to emulate
work_load=total number of terms to calculate
arrival_times=arrival time for every process to emulate(in ms)
burst_time=how many terms will be calculated before the process returns the cpu to the scheduler
initial_priority=(only used for PS, PS_RR)
IO_block=time in ms for the process to wait in the waiting queue
quantum=(only used in RR and PS_RR)
```
now you can run your example with:
```bash
./Scheduler route_to_your_example/example.txt
```
