/*
* Scheduler.c - emulador de algoritmos de calendarizacion
* Yarol Montoya <yarolmj@gmail.com>
*/

#include <gtk/gtk.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>

gboolean Calculando = FALSE;
gboolean console_log = TRUE;

static GMutex time_lock;

#define PARM_COUNT 7

typedef struct process
{
  //Scheduler
  int pid;
  uint64_t params[PARM_COUNT];
  double pi_val;
  uint8_t state;
  gboolean new;
  //UI
  GtkWidget *progressBar;
  GtkWidget *pi_label;
  GtkWidget *proc_label;
}process;

//Base Structure for linked list handling
typedef struct queue_node
{
  process *proc;
  struct queue_node *next;
}queue_node;

enum process_state{
  S_ARRIVAL = 0,
  S_READY = 1,
  S_ACTIVE = 2,
  S_IOBLOCK = 3,
  S_DONE = 4
};

enum process_param{
  P_ARRIVAL_TIME = 0,
  P_WORK_LOAD = 1,
  P_BURST_TIME = 2,
  P_WORK_DONE = 3,
  P_PRIORITY = 4,
  P_WAIT_TIME = 5,
  P_IO_BLOCK = 6
};

enum algorithms {
  FCFS = 1,
  SJF = 2,
  RR = 3,
  PS = 4,
  PS_RR = 5,
  MQS = 6,
  MFQS = 7
};

enum EXPROP_REASONS{
  IO_EXPROP = 0,
  QUANTUM_EXPROP = 1,
  WLFINISH_EXPROP = 2,
  HP_EXPROP = 3
};

GtkWidget *start_button;

queue_node *process_list; //General process list, use for UI updates

//Parametros de funcionamiento
uint8_t algorithm = 0;
uint8_t expropiative_mode = 0;
uint8_t process_count = 5;
uint64_t quantum = 100;

//Time handling variables
uint64_t time_counter = 0; //Actual number of miliseconds elapsed
int time_inc = 1; //Step size for the *time_counter* variable

//Thread function for time update
gpointer UpdateTime(int *time_inc){
  g_mutex_lock(&time_lock);
  struct timespec wait_time;
  wait_time.tv_sec = 0;
  wait_time.tv_nsec =1000000;
  while(1){
    time_counter+=*time_inc;
    clock_nanosleep(CLOCK_REALTIME,0,&wait_time,NULL);
  }
  return NULL;
}
//interger to string converter
char *itoa(const int number){
  char *text_buffer = (char*)malloc(16);
  sprintf(text_buffer,"%i",number);
  return text_buffer;
}
//long float to string converter
char *ftoa(const double number){
  char *text_buffer = (char*)malloc(16);
  sprintf(text_buffer,"%.12lf",number);
  return text_buffer;
}

//push a *new_proc* to the head of *head_ref* queue
void push_process(queue_node** head_ref,process *new_proc){
  queue_node* new_node = (queue_node*)malloc(sizeof(queue_node));
  new_node->proc = new_proc;
  new_node->next = (*head_ref);
  (*head_ref) = new_node;
}
//extracts a process in *pos* position from *head_ref* queue
process *pop_process(queue_node** head_ref,int pos){
  queue_node *prev = NULL;
  queue_node *temp= *head_ref;
  process *proc;
  int actual_pos = 0;
  //Es el primero
  if(pos == actual_pos){
    *head_ref = temp->next;
    proc = temp->proc;
    free(temp);
  }else{ //No es el primero
    while(actual_pos != pos){
      prev = temp;
      temp = temp->next;
      actual_pos++;
    }
    prev->next = temp->next;
    
    proc = temp->proc;
    free(temp);
  }
  return proc;
}

//Prints a list of the processes linked to a *head_ref* list
void print_list(queue_node** head_ref){
  queue_node *temp = *head_ref;
  while(temp != NULL)
  {
    printf(" [pid] = %i \t| at = %li \t| wl = %li \t| bt = %li \t| p = %li \t| io = %li\n",temp->proc->pid,temp->proc->params[P_ARRIVAL_TIME],temp->proc->params[P_WORK_LOAD],temp->proc->params[P_BURST_TIME],temp->proc->params[P_PRIORITY],temp->proc->params[P_IO_BLOCK]);
    temp = temp->next;
  }
}

//Converts a string with coma separed values to an array of integers
void string2array(char *input_string,uint64_t *array){
  int element = 0;
  int main_index = 0;
  while (input_string[main_index] != '\0')
  {    
    int sub_index = 0;
    char number[16];
    memset(&number,0,16);
    while(input_string[main_index+sub_index] != ',' && input_string[main_index+sub_index] != '\0'){
      number[sub_index] = input_string[main_index+sub_index];
      sub_index++;
    }
    array[element] = atol(number);
    element++;
    main_index+=sub_index;
    main_index++;
  }
}

//Loads and processes the configuration file for the scheduler to run
void config_parser(char *config_path){
  FILE *config_file = fopen(config_path,"r");
  char format_buffer[340];
  memset(format_buffer,0,340);
  fscanf(config_file,"algorithm=%s\n",&format_buffer[0]);
  if(strcmp("FCFS",format_buffer) == 0){
    printf("algoritm=FCFS\n");
    algorithm = FCFS;
  }
  if(strcmp("SJF",format_buffer) == 0){
    printf("algoritm=SJF\n");
    algorithm = SJF;
  }
  if(strcmp("RR",format_buffer) == 0){
    printf("algoritm=RR\n");
    algorithm = RR;
  }
  if(strcmp("PS",format_buffer) == 0){
    printf("algoritm=PS\n");
    algorithm = PS;
  }
  if(strcmp("PS_RR",format_buffer) == 0){
    printf("algoritm=PS_RR\n");
    algorithm = PS_RR;
  }
  if(strcmp("MQS",format_buffer) == 0){
    printf("algoritm=MQS\n");
    algorithm = MQS;
  }
  if(strcmp("MFQS",format_buffer) == 0){
    printf("algoritm=MFQS\n");
    algorithm = MFQS;
  }
  fscanf(config_file,"expropiative_mode=%hhd\n",&expropiative_mode);
  printf("expropiative_mode=%i\n",expropiative_mode);
  fscanf(config_file,"process_count=%hhd\n",&process_count);
  printf("process_count=%i\n",process_count);
  
  for (size_t i = 0; i < process_count; i++)
  {
    process* temp = (process*)malloc(sizeof(process));
    //Inicializa procesos
    temp->state = S_ARRIVAL;
    temp->new = FALSE;
    temp->pid = i+4000;
    temp->pi_val = 0.0;
    temp->params[P_WORK_DONE] = 0;
    temp->params[P_WAIT_TIME] = 0;
    temp->progressBar = gtk_progress_bar_new();
    gtk_progress_bar_set_show_text(GTK_PROGRESS_BAR(temp->progressBar),TRUE);
    temp->pi_label = gtk_label_new("0.00000000000");
    char *text_buffer = (char*)malloc(9);
    sprintf(text_buffer,"PID:%i",temp->pid);
    temp->proc_label = gtk_label_new(text_buffer);
    free(text_buffer);
    push_process(&process_list,temp);
  }

  uint64_t *wl_array = (uint64_t*)malloc(sizeof(uint64_t)*process_count);
  uint64_t *at_array = (uint64_t*)malloc(sizeof(uint64_t)*process_count);
  uint64_t *brst_array = (uint64_t*)malloc(sizeof(uint64_t)*process_count);
  uint64_t *prio_array = (uint64_t*)malloc(sizeof(uint64_t)*process_count);
  uint64_t *io_array = (uint64_t*)malloc(sizeof(uint64_t)*process_count);

  memset(&format_buffer,0,340);
  fscanf(config_file,"work_load=%s\n",&format_buffer[0]);
  string2array(format_buffer,wl_array);

  memset(&format_buffer,0,340);
  fscanf(config_file,"arrival_times=%s\n",&format_buffer [0]);
  string2array(format_buffer,at_array);

  memset(&format_buffer,0,340);
  fscanf(config_file,"burst_time=%s\n",&format_buffer [0]);
  string2array(format_buffer,brst_array);
  
  memset(&format_buffer,0,340);
  fscanf(config_file,"initial_priority=%s\n",&format_buffer [0]);
  string2array(format_buffer,prio_array);

  memset(&format_buffer,0,340);
  fscanf(config_file,"IO_block=%s\n",&format_buffer [0]);
  string2array(format_buffer,io_array);

  int temp_index = 0;
  queue_node *temp_node = process_list;
  while(temp_node != NULL){
    temp_node->proc->params[P_WORK_LOAD] = wl_array[temp_index];
    temp_node->proc->params[P_ARRIVAL_TIME] = at_array[temp_index];
    temp_node->proc->params[P_BURST_TIME] = brst_array[temp_index];
    temp_node->proc->params[P_PRIORITY] = prio_array[temp_index];
    temp_node->proc->params[P_IO_BLOCK] = io_array[temp_index];

    temp_index++;
    temp_node = temp_node->next;
  }
  fscanf(config_file,"quantum=%ld\n",&quantum);
  printf("quantum=%li\n",quantum);
  printf("---------PROCESOS CARGADOS---------\n");
  print_list(&process_list);
  free(wl_array);
  free(at_array);
  free(brst_array);
  free(prio_array);
  fclose(config_file);
}
//Algoritm for pi calculus 
double pi_Madhava(uint64_t initial_k){
  double sum = 0.0;
  for (size_t i = initial_k; i < initial_k+50; i++)
  {
    sum+=sqrt(12.0)*(pow(-3.0,-(double)i))/(2.0*(double)i+1.0);
  }
  return sum;
}

//Uses bubble sorth algoritm to rearrange a list of linked process using *key* process parameter as criteria for ascending order.
void rearrange_queue(queue_node **head_node,int key){
  gboolean swap = TRUE;
  do{
    swap = FALSE;
    queue_node *temp_node = *head_node;
    process *temp_proc;

    while(temp_node->next != NULL){
      if(temp_node->proc->params[key] > temp_node->next->proc->params[key]){
        temp_proc = temp_node->proc;
        temp_node->proc = temp_node->next->proc;
        temp_node->next->proc = temp_proc;
        swap = TRUE;
      }
      temp_node = temp_node->next;
    }
  }while(swap);
}
//Has the process in *proc finish all his work load?
static gboolean check_wip(process *proc){
  if(proc->params[P_WORK_LOAD] - proc->params[P_WORK_DONE] > 0){
    return TRUE;
  }
  return FALSE;
}

//Copies a linked list and returns a pointer to the new duplicate 
queue_node *copy_queue(queue_node **head_node){
  queue_node *temp = *head_node;
  queue_node *new_queue = NULL;
  while (temp != NULL)
  {
    push_process(&new_queue,temp->proc);
    temp = temp->next;
  }
  return new_queue;
}
//Dinamicaly feeds new processes from one queue to another acording to a time *parameter*
gboolean feed_queue(queue_node **wait_queue,queue_node **to_queue,int parameter){
  //La cola de alimentación esta vacia
  if(*wait_queue == NULL){
    return FALSE;
  }
  gboolean feeded = FALSE;
  //No esta vacia, hay que determinar si algun proceso ha llegado, y si lo ha hecho agregarlo al final de "to_queue"
  queue_node *temp_node = *wait_queue;
  uint64_t actual_time = time_counter; //Guarda el tiempo actual, evita condiciones de carrera o lecturas de tiempo variantes durante la funcion
  int pos = 0;
  while (temp_node != NULL){
    uint64_t spend_time = actual_time-temp_node->proc->params[P_WAIT_TIME];
    if(spend_time >= temp_node->proc->params[parameter])
    {
      process *temp_proc = pop_process(wait_queue,pos);
      temp_proc->state = S_READY;
      temp_proc->new = TRUE;
      temp_proc->params[P_ARRIVAL_TIME] = time_counter;
      push_process(to_queue,temp_proc);
      feeded = TRUE;
      temp_node = *wait_queue;
      pos = 0;
    }else{
      pos++;
      temp_node = temp_node->next;
    }
    
  }
  return feeded;
}
//Dinamicaly feeds new processes from one queue to 3 another acording to a time *parameter* and process priority
gboolean feed_queue_MQ(queue_node **wait_queue,queue_node **to_queue_0,queue_node **to_queue_1,queue_node **to_queue_2,int parameter){
  //La cola de alimentación esta vacia
  if(*wait_queue == NULL){
    return FALSE;
  }
  gboolean feeded = FALSE;
  //No esta vacia, hay que determinar si algun proceso ha llegado, y si lo ha hecho agregarlo al final de "to_queue"
  queue_node *temp_node = *wait_queue;
  uint64_t actual_time = time_counter; //Guarda el tiempo actual, evita condiciones de carrera o lecturas de tiempo variantes durante la funcion
  int pos = 0;
  while (temp_node != NULL){
    uint64_t spend_time = actual_time-temp_node->proc->params[P_WAIT_TIME];
    
    if(spend_time >= temp_node->proc->params[parameter])
    {
      temp_node->proc->state = S_READY;
      process *temp_proc = pop_process(wait_queue,pos);
      switch (temp_proc->params[P_PRIORITY])
      {
      case 0:
        push_process(to_queue_0,temp_proc);
        break;
      case 1:
        push_process(to_queue_1,temp_proc);
        break;
      case 2:
        push_process(to_queue_2,temp_proc);
        break;
      default:
        break;
      }
      feeded = TRUE;
      temp_node = *wait_queue;
      pos = 0;
    }else{
      pos++;
      temp_node = temp_node->next;
    }
  }
  return feeded;
}

//Corre el proceso proc por 1 unidad de trabajo
void run(process *proc){
  proc->pi_val += pi_Madhava((uint64_t)proc->params[P_WORK_DONE]*50); //Calcula 50 terminos
  proc->params[P_WORK_DONE]++;
}

//Scheduling Algoritms 

gpointer _MFQS(gpointer *data){
  queue_node **process_data = (queue_node **)data;
  rearrange_queue(process_data,P_ARRIVAL_TIME);
  queue_node *process_queue = copy_queue(process_data);
  //Todos los procesos empiezan en prioridad 0
  queue_node *temp_node = process_queue;
  while (temp_node != NULL)
  {
    temp_node->proc->params[P_PRIORITY] = 0;
    temp_node = temp_node->next;
  }
  
  rearrange_queue(&process_queue,P_ARRIVAL_TIME);

  queue_node *ready_queues[3] = {NULL};

  queue_node *waiting_queue = NULL; //Cola para procesos ya ejecutados pero no terminados de prioridad 0

  gboolean ready_left = ready_queues[0] != NULL || ready_queues[1] != NULL || ready_queues[2] != NULL;
  time_counter = 0;
  g_mutex_unlock(&time_lock);
  while (waiting_queue != NULL || ready_left || process_queue != NULL){
    feed_queue_MQ(&process_queue,&ready_queues[0],&ready_queues[1],&ready_queues[2],P_ARRIVAL_TIME);
    feed_queue_MQ(&waiting_queue,&ready_queues[0],&ready_queues[1],&ready_queues[2],P_IO_BLOCK);
    
    uint8_t exprop_reason = IO_EXPROP;
    uint64_t MQS_time;
    process *temp_proc = NULL;
    //Cola 1 Round Robin quantum = 8 ms
    if(ready_queues[0] != NULL){
      rearrange_queue(&ready_queues[0],P_ARRIVAL_TIME);
      temp_proc = pop_process(&ready_queues[0],0);
      MQS_time = time_counter;
      temp_proc->state = S_ACTIVE;
      for (uint64_t i = 0; i < temp_proc->params[P_BURST_TIME]; i++)
      {
        //Se acabo el quantum
        if(time_counter-MQS_time > 8){
          temp_proc->params[P_PRIORITY] = 1; //Baja a prioridad 1
          exprop_reason = QUANTUM_EXPROP;
          break;
        }
        //El trabajo terminó, mantiene la prioridad
        if(check_wip(temp_proc)){
          run(temp_proc);
        }else{
          exprop_reason = WLFINISH_EXPROP;
          break;
        }
      }
      
    }else if (ready_queues[1] != NULL)
    {
      rearrange_queue(&ready_queues[1],P_ARRIVAL_TIME);
      temp_proc = pop_process(&ready_queues[1],0);
      MQS_time = time_counter;
      temp_proc->state = S_ACTIVE;
      for (uint64_t i = 0; i < temp_proc->params[P_BURST_TIME]; i++)
      {
        //Se acabo el quantum, baja a prioridad 2
        if(time_counter-MQS_time > 24){
          temp_proc->params[P_PRIORITY] = 2;
          exprop_reason = QUANTUM_EXPROP;
          break;
        }
        if(check_wip(temp_proc)){
          run(temp_proc);
        }else{
          //El trabajo terminó, mantiene prioridad
          exprop_reason = WLFINISH_EXPROP;
          break;
        }
        //Llego otro proceso a una cola de mayor prioridad
        feed_queue_MQ(&process_queue,&ready_queues[0],&ready_queues[1],&ready_queues[2],P_ARRIVAL_TIME);
        feed_queue_MQ(&waiting_queue,&ready_queues[0],&ready_queues[1],&ready_queues[2],P_IO_BLOCK);
        if(ready_queues[0] != NULL && expropiative_mode){
          exprop_reason = HP_EXPROP;
          break;
        }
      }
      //Termino el burst en menos de 8 ms, le queda trabajo y no fue expropiado por un proceso de mayor prioridad => sube a prioridad 0
      if(time_counter-MQS_time <= 8 && check_wip(temp_proc) && exprop_reason == IO_EXPROP){
          temp_proc->params[P_PRIORITY] = 0;
      }
    }else if (ready_queues[2] != NULL)
    {
      rearrange_queue(&ready_queues[2],P_ARRIVAL_TIME);
      temp_proc = pop_process(&ready_queues[2],0);
      MQS_time = time_counter;
      temp_proc->state = S_ACTIVE;
      for (uint64_t i = 0; i < temp_proc->params[P_BURST_TIME]; i++)
      {
        //El trabajo terminó
        if(check_wip(temp_proc)){
          run(temp_proc);
        }else{
          exprop_reason = WLFINISH_EXPROP;
          break;
        }
        //Llego otro proceso a una cola de mayor prioridad
        feed_queue_MQ(&process_queue,&ready_queues[0],&ready_queues[1],&ready_queues[2],P_ARRIVAL_TIME);
        feed_queue_MQ(&waiting_queue,&ready_queues[0],&ready_queues[1],&ready_queues[2],P_IO_BLOCK);
        //En cuanto aparece un proceso de mayor prioridad este proceso es expropiado
        if((ready_queues[0] != NULL || ready_queues[1] != NULL) && expropiative_mode){
          exprop_reason = HP_EXPROP;
          break;
        }
      }
      //Termino su burst en menos de 24 ms y le resta trabajo sube de prioridad
      if(time_counter-MQS_time < 24 && check_wip(temp_proc) && exprop_reason == IO_EXPROP){
            temp_proc->params[P_PRIORITY] = 1;
      }
    }
    if(temp_proc != NULL){
      switch (exprop_reason)
      {
      case IO_EXPROP:
        temp_proc->state = S_IOBLOCK;
        //Se manda el proceso a la cola de waiting hasta que reciba el IO(Simulado)
        temp_proc->params[P_WAIT_TIME] = time_counter;
        push_process(&waiting_queue,temp_proc);
        break;
      case QUANTUM_EXPROP:
        temp_proc->state = S_READY;
        //Vuelve a la cola de ready
        temp_proc->params[P_ARRIVAL_TIME] = time_counter;
        push_process(&ready_queues[temp_proc->params[P_PRIORITY]],temp_proc);
        break;
      case WLFINISH_EXPROP:
        temp_proc->state = S_DONE;
        if(console_log){
            printf("PID:%i Terminado.\n",temp_proc->pid);
          }
        //No hace nada, el puntero no se necesita en ninguna cola
        break;
      case HP_EXPROP:
        temp_proc->state = S_READY;
        //Expropiado por mayor prioridad => lo envia a la cola de ready y no cambia su prioridad
        temp_proc->params[P_ARRIVAL_TIME] = time_counter;
        push_process(&ready_queues[temp_proc->params[P_PRIORITY]],temp_proc);
        break;
      default:
        break;
      }
    }
    ready_left = ready_queues[0] != NULL || ready_queues[1] != NULL || ready_queues[2] != NULL;
  }

  Calculando = FALSE;
  printf("Tiempo:%li\n",time_counter);
  print_list(process_data);
  return NULL;
}
void _MFQS_INIT(GtkButton *button,gpointer *data){
  if(Calculando == FALSE){
    Calculando = TRUE;
    gtk_button_set_label(button,"WORKING");
    g_thread_new("thread",(GThreadFunc)_MFQS,data);
    g_thread_new("time",(GThreadFunc)UpdateTime,(gpointer)&time_inc);
  }
}


gpointer _MQS(gpointer *data){
  queue_node **process_data = (queue_node **)data;
  rearrange_queue(process_data,P_ARRIVAL_TIME);
  queue_node *process_queue = copy_queue(process_data);
  rearrange_queue(&process_queue,P_ARRIVAL_TIME);

  queue_node *ready_queues[3] = {NULL};

  queue_node *waiting_queue = NULL; //Cola para procesos ya ejecutados pero no terminados de prioridad 0

  gboolean ready_left = ready_queues[0] != NULL || ready_queues[1] != NULL || ready_queues[2] != NULL;
  time_counter = 0;
  g_mutex_unlock(&time_lock);
  while (waiting_queue != NULL || ready_left || process_queue != NULL){
    feed_queue_MQ(&process_queue,&ready_queues[0],&ready_queues[1],&ready_queues[2],P_ARRIVAL_TIME);
    feed_queue_MQ(&waiting_queue,&ready_queues[0],&ready_queues[1],&ready_queues[2],P_IO_BLOCK);
    
    uint8_t exprop_reason = IO_EXPROP;
    uint64_t MQS_time;
    process *temp_proc = NULL;
    //Cola 1 Round Robin quantum = 8 ms
    if(ready_queues[0] != NULL){
      rearrange_queue(&ready_queues[0],P_ARRIVAL_TIME);
      temp_proc = pop_process(&ready_queues[0],0);
      MQS_time = time_counter;
      temp_proc->state = S_ACTIVE;
      for (uint64_t i = 0; i < temp_proc->params[P_BURST_TIME]; i++)
      {
        //Se acabo el quantum
        if(time_counter-MQS_time > 8){
          exprop_reason = QUANTUM_EXPROP;
          break;
        }
        //El trabajo terminó
        if(check_wip(temp_proc)){
          run(temp_proc);
        }else{
          exprop_reason = WLFINISH_EXPROP;
          break;
        }
      }
      
    }else if (ready_queues[1] != NULL)
    {
      rearrange_queue(&ready_queues[1],P_ARRIVAL_TIME);
      temp_proc = pop_process(&ready_queues[1],0);
      MQS_time = time_counter;
      temp_proc->state = S_ACTIVE;
      for (uint64_t i = 0; i < temp_proc->params[P_BURST_TIME]; i++)
      {
        //Se acabo el quantum
        if(time_counter-MQS_time > 24){
          exprop_reason = QUANTUM_EXPROP;
          break;
        }
        if(check_wip(temp_proc)){
          run(temp_proc);
        }else{
          //El trabajo terminó
          exprop_reason = WLFINISH_EXPROP;
          break;
        }
        //Llego otro proceso a una cola de mayor prioridad
        feed_queue_MQ(&process_queue,&ready_queues[0],&ready_queues[1],&ready_queues[2],P_ARRIVAL_TIME);
        feed_queue_MQ(&waiting_queue,&ready_queues[0],&ready_queues[1],&ready_queues[2],P_IO_BLOCK);
        if(ready_queues[0] != NULL && expropiative_mode){
          exprop_reason = HP_EXPROP;
          break;
        }
      }
    }else if (ready_queues[2] != NULL)
    {
      rearrange_queue(&ready_queues[2],P_ARRIVAL_TIME);
      temp_proc = pop_process(&ready_queues[2],0);
      MQS_time = time_counter;
      temp_proc->state = S_ACTIVE;
      for (uint64_t i = 0; i < temp_proc->params[P_BURST_TIME]; i++)
      {
        //El trabajo terminó
        if(check_wip(temp_proc)){
          run(temp_proc);
        }else{
          exprop_reason = WLFINISH_EXPROP;
          break;
        }
        //Llego otro proceso a una cola de mayor prioridad
        feed_queue_MQ(&process_queue,&ready_queues[0],&ready_queues[1],&ready_queues[2],P_ARRIVAL_TIME);
        feed_queue_MQ(&waiting_queue,&ready_queues[0],&ready_queues[1],&ready_queues[2],P_IO_BLOCK);
        //En cuanto aparece un proceso de mayor prioridad este proceso es expropiado
        if((ready_queues[0] != NULL || ready_queues[1] != NULL) && expropiative_mode){
          exprop_reason = HP_EXPROP;
          break;
        }
      }
    }
    if(temp_proc != NULL){
      switch (exprop_reason)
      {
      case IO_EXPROP:
        temp_proc->state = S_IOBLOCK;
        //Se manda el proceso a la cola de waiting hasta que reciba el IO(Simulado)
        temp_proc->params[P_WAIT_TIME] = time_counter;
        push_process(&waiting_queue,temp_proc);
        break;
      case QUANTUM_EXPROP:
        temp_proc->state = S_READY;
        //Vuelve a la cola de ready
        temp_proc->params[P_ARRIVAL_TIME] = time_counter;
        push_process(&ready_queues[temp_proc->params[P_PRIORITY]],temp_proc);
        break;
      case WLFINISH_EXPROP:
        temp_proc->state = S_DONE;
        if(console_log){
            printf("PID:%i Terminado.\n",temp_proc->pid);
          }
        //No hace nada, el puntero no se necesita en ninguna cola
        break;
      case HP_EXPROP:
        temp_proc->state = S_READY;
        //Expropiado por mayor prioridad => lo envia a la cola de ready
        temp_proc->params[P_ARRIVAL_TIME] = time_counter;
        push_process(&ready_queues[temp_proc->params[P_PRIORITY]],temp_proc);
        break;
      default:
        break;
      }
    }
    ready_left = ready_queues[0] != NULL || ready_queues[1] != NULL || ready_queues[2] != NULL;
  }

  Calculando = FALSE;
  printf("Tiempo:%li\n",time_counter);
  return NULL;
}

void _MQS_INIT(GtkButton *button,gpointer *data){
  if(Calculando == FALSE){
    Calculando = TRUE;
    gtk_button_set_label(button,"WORKING");
    g_thread_new("thread",(GThreadFunc)_MQS,data);
    g_thread_new("time",(GThreadFunc)UpdateTime,(gpointer)&time_inc);
  }
}

gpointer _PS(gpointer *data){
  queue_node **process_data = (queue_node **)data;
  rearrange_queue(process_data,P_ARRIVAL_TIME);
  queue_node *process_queue = copy_queue(process_data);
  rearrange_queue(&process_queue,P_ARRIVAL_TIME);

  queue_node *ready_queue = NULL;   //Cola para procesos a espera de ejecución
  queue_node *waiting_queue = NULL; //Cola para procesos ya ejecutados pero no terminados

  time_counter = 0;
  g_mutex_unlock(&time_lock);
  while (process_queue != NULL || ready_queue != NULL || waiting_queue != NULL) //Van a llegar procesos o la cola de ready aun tiene trabajo faltante o hay procesos en waiting
  {
    //Operaciones de actualizacion de colas
    feed_queue(&process_queue,&ready_queue,P_ARRIVAL_TIME);
    feed_queue(&waiting_queue,&ready_queue,P_IO_BLOCK);

    process *temp_proc = NULL;
    uint8_t exprop_reason = IO_EXPROP;

    if(ready_queue != NULL) //Hay algo en la cola de ready, ejecuto el primer proceso de la cola
    {
      
      //Reorganizo la cola de ready segun el criterio de calendarización y si no por tiempo de llegada
      rearrange_queue(&ready_queue,P_ARRIVAL_TIME);
      rearrange_queue(&ready_queue,P_PRIORITY);
      //print_list(&ready_queue);
      temp_proc = pop_process(&ready_queue,0);
      temp_proc->state = S_ACTIVE;
      for (uint64_t i = 0; i < temp_proc->params[P_BURST_TIME]; i++)
        {
          if(check_wip(temp_proc)){
            //El proceso no ha terminado todo su trabajo
            gboolean queue_change = feed_queue(&process_queue,&ready_queue,P_ARRIVAL_TIME) || feed_queue(&waiting_queue,&ready_queue,P_IO_BLOCK);
            if(queue_change && expropiative_mode)
            {
              //Llego un nuevo proceso, hay que revisar si tiene mayor prioridad sobre el actual
              rearrange_queue(&ready_queue,P_PRIORITY);
              if(ready_queue->proc->params[P_PRIORITY] < temp_proc->params[P_PRIORITY]){
                exprop_reason = HP_EXPROP;
                break; //Llego nuevo proceso con mayor prioridad, rompa el ciclo y vuelva a empezar
              }
            }
            //Ejecuta el trabajo correspondiente
            run(temp_proc);
          }else{
            exprop_reason = WLFINISH_EXPROP;
            break; //Proceso completado
          }
        }
    }
    if(temp_proc != NULL){
      switch (exprop_reason)
      {
      case IO_EXPROP:
        temp_proc->state = S_IOBLOCK;
        temp_proc->params[P_WAIT_TIME] = time_counter;
        push_process(&waiting_queue,temp_proc);
        break;
      case HP_EXPROP:
        temp_proc->state = S_READY;
        temp_proc->params[P_ARRIVAL_TIME] = time_counter;
        push_process(&ready_queue,temp_proc);
        break;
      case WLFINISH_EXPROP:
        temp_proc->state = S_DONE;
        if(console_log){
            printf("PID:%i Terminado.\n",temp_proc->pid);
          }
        break;
      default:
        break;
      }
    }  
  }
  Calculando = FALSE;
  printf("Tiempo:%li\n",time_counter);
  return NULL;
}

void _PS_INIT(GtkButton *button,gpointer *data){
  if(Calculando == FALSE){
    Calculando = TRUE;
    gtk_button_set_label(button,"WORKING");
    g_thread_new("thread",(GThreadFunc)_PS,data);
    g_thread_new("time",(GThreadFunc)UpdateTime,(gpointer)&time_inc);
  }
}

gpointer _PS_RR(gpointer *data){
  queue_node **process_data = (queue_node **)data;
  rearrange_queue(process_data,P_ARRIVAL_TIME);
  queue_node *process_queue = copy_queue(process_data);
  rearrange_queue(&process_queue,P_ARRIVAL_TIME);

  queue_node *ready_queue = NULL;   //Cola para procesos a espera de ejecución
  queue_node *waiting_queue = NULL; //Cola para procesos ya ejecutados pero no terminados

  time_counter = 0;
  g_mutex_unlock(&time_lock);
  while (process_queue != NULL || ready_queue != NULL || waiting_queue != NULL) //Van a llegar procesos o la cola de ready aun tiene trabajo faltante o hay procesos en waiting
  {
    //Operaciones de actualizacion de colas
    feed_queue(&process_queue,&ready_queue,P_ARRIVAL_TIME);
    feed_queue(&waiting_queue,&ready_queue,P_IO_BLOCK);

    process *temp_proc = NULL;
    uint8_t exprop_reason = IO_EXPROP;

    if(ready_queue != NULL) //Hay algo en la cola de ready, ejecuto el primer proceso de la cola
    {
      
      //Reorganizo la cola de ready segun el criterio de calendarización y si no por tiempo de llegada
      rearrange_queue(&ready_queue,P_ARRIVAL_TIME);
      rearrange_queue(&ready_queue,P_PRIORITY);
      //print_list(&ready_queue);
      temp_proc = pop_process(&ready_queue,0);
      temp_proc->state = S_ACTIVE;
      uint64_t PS_RR_TIME = time_counter;
      for (uint64_t i = 0; i < temp_proc->params[P_BURST_TIME]; i++)
        {
          if(time_counter - PS_RR_TIME > quantum){
            exprop_reason = QUANTUM_EXPROP;
            break;
          }
          if(check_wip(temp_proc)){
            //El proceso no ha terminado todo su trabajo
            gboolean queue_change = feed_queue(&process_queue,&ready_queue,P_ARRIVAL_TIME) || feed_queue(&waiting_queue,&ready_queue,P_IO_BLOCK);
            if(queue_change && expropiative_mode)
            {
              //Reviso todos los nodos nuevos buscando una prioridad mayor
              queue_node *examinator_node = ready_queue;
              while (examinator_node != NULL)
              {
                if(examinator_node->proc->new){
                  examinator_node->proc->new = FALSE;
                  if(examinator_node->proc->params[P_PRIORITY] < temp_proc->params[P_PRIORITY])
                  {
                    exprop_reason = HP_EXPROP;
                  }
                }
                examinator_node = examinator_node->next;
              }
              if(exprop_reason == HP_EXPROP){
                break;
              }
            }
            //Ejecuta el trabajo correspondiente
            run(temp_proc);
          }else{
            exprop_reason = WLFINISH_EXPROP;
            break; //Proceso completado
          }
        }
    }
    if(temp_proc != NULL){
      switch (exprop_reason)
      {
      case QUANTUM_EXPROP:
        temp_proc->state = S_READY;
        temp_proc->params[P_ARRIVAL_TIME] = time_counter;
        push_process(&ready_queue,temp_proc);
        break;
      case IO_EXPROP:
        temp_proc->state = S_IOBLOCK;
        temp_proc->params[P_WAIT_TIME] = time_counter;
        push_process(&waiting_queue,temp_proc);
        break;
      case HP_EXPROP:
        temp_proc->state = S_READY;
        temp_proc->params[P_ARRIVAL_TIME] = time_counter;
        push_process(&ready_queue,temp_proc);
        break;
      case WLFINISH_EXPROP:
        temp_proc->state = S_DONE;
        if(console_log){
            printf("PID:%i Terminado.\n",temp_proc->pid);
          }
        break;
      default:
        break;
      }
    }  
  }
  Calculando = FALSE;
  printf("Tiempo:%li\n",time_counter);
  return NULL;
}

void _PS_RR_INIT(GtkButton *button,gpointer *data){
  if(Calculando == FALSE){
    Calculando = TRUE;
    gtk_button_set_label(button,"WORKING");
    g_thread_new("thread",(GThreadFunc)_PS_RR,data);
    g_thread_new("time",(GThreadFunc)UpdateTime,(gpointer)&time_inc);
  }
}

gpointer _SJF(gpointer *data){
  queue_node **process_data = (queue_node **)data;
  rearrange_queue(process_data,P_ARRIVAL_TIME);
  queue_node *process_queue = copy_queue(process_data);
  rearrange_queue(&process_queue,P_ARRIVAL_TIME);

  queue_node *ready_queue = NULL;   //Cola para procesos a espera de ejecución
  queue_node *waiting_queue = NULL; //Cola para procesos ya ejecutados pero no terminados

  time_counter = 0;
  g_mutex_unlock(&time_lock);
  while (process_queue != NULL || ready_queue != NULL || waiting_queue != NULL) //Van a llegar procesos o la cola de ready aun tiene trabajo faltante o hay procesos en waiting
  {
    //Operaciones de actualizacion de colas
    feed_queue(&process_queue,&ready_queue,P_ARRIVAL_TIME);
    feed_queue(&waiting_queue,&ready_queue,P_IO_BLOCK);

    process *temp_proc = NULL;
    uint8_t exprop_reason = IO_EXPROP;

    if(ready_queue != NULL) //Hay algo en la cola de ready, ejecuto el primer proceso de la cola
    {
      
      //Reorganizo la cola de ready segun el criterio de calendarización y si no por tiempo de llegada
      rearrange_queue(&ready_queue,P_ARRIVAL_TIME);
      rearrange_queue(&ready_queue,P_BURST_TIME);
      //print_list(&ready_queue);
      temp_proc = pop_process(&ready_queue,0);
      temp_proc->state = S_ACTIVE;
      for (uint64_t i = 0; i < temp_proc->params[P_BURST_TIME]; i++)
      {
        if(check_wip(temp_proc)){
          //El proceso no ha terminado todo su trabajo
          gboolean queue_change = feed_queue(&process_queue,&ready_queue,P_ARRIVAL_TIME) || feed_queue(&waiting_queue,&ready_queue,P_IO_BLOCK);
          //Posible expropiacion
          if(queue_change && expropiative_mode)
          {
            //Reviso todos los nodos nuevos buscando burst mas cortos
            queue_node *examinator_node = ready_queue;
            while (examinator_node != NULL)
            {
              if(examinator_node->proc->new){
                examinator_node->proc->new = FALSE;
                if(examinator_node->proc->params[P_BURST_TIME] < (temp_proc->params[P_BURST_TIME]-i))
                {
                  exprop_reason = HP_EXPROP;
                }
              }
              examinator_node = examinator_node->next;
            }
            if(exprop_reason == HP_EXPROP){
              break;
            }
          }
          run(temp_proc);
        }else{
          exprop_reason = WLFINISH_EXPROP;
          break; //Proceso completado
        }
      }
    }
    if(temp_proc != NULL){
      switch (exprop_reason)
      {
      case IO_EXPROP:
        temp_proc->state = S_IOBLOCK;
        temp_proc->params[P_WAIT_TIME] = time_counter;
        push_process(&waiting_queue,temp_proc);
        break;
      case HP_EXPROP:
        temp_proc->state = S_READY;
        temp_proc->params[P_ARRIVAL_TIME] = time_counter;
        push_process(&ready_queue,temp_proc);
        break;
      case WLFINISH_EXPROP:
        temp_proc->state = S_DONE;
        //No se hace nada con las colas
        if(console_log){
            printf("PID:%i Terminado.\n",temp_proc->pid);
          }
        break;
      default:
        break;
      }
    }    
  }
  Calculando = FALSE;
  printf("Tiempo:%li\n",time_counter);
  return NULL;
}

void _SJF_INIT(GtkButton *button,gpointer *data){
  if(Calculando == FALSE){
    Calculando = TRUE;
    gtk_button_set_label(button,"WORKING");
    g_thread_new("thread",(GThreadFunc)_SJF,data);
    g_thread_new("time",(GThreadFunc)UpdateTime,(gpointer)&time_inc);
  }
}

gpointer _RR(gpointer *data){
  queue_node **process_data = (queue_node **)data;
  rearrange_queue(process_data,P_ARRIVAL_TIME);
  queue_node *process_queue = copy_queue(process_data);
  rearrange_queue(&process_queue,P_ARRIVAL_TIME);

  queue_node *ready_queue = NULL;   //Cola para procesos a espera de ejecución
  queue_node *waiting_queue = NULL; //Cola para procesos ya ejecutados pero no terminados

  time_counter = 0;
  g_mutex_unlock(&time_lock);
  while (process_queue != NULL || ready_queue != NULL || waiting_queue != NULL) //Van a llegar procesos o la cola de ready aun tiene trabajo faltante o hay procesos en waiting
  {
    //Operaciones de actualizacion de colas
    feed_queue(&process_queue,&ready_queue,P_ARRIVAL_TIME);
    feed_queue(&waiting_queue,&ready_queue,P_IO_BLOCK);

    process *temp_proc = NULL;
    uint8_t exprop_reason = IO_EXPROP;

    if(ready_queue != NULL) //Hay algo en la cola de ready, ejecuto el primer proceso de la cola
    {
      //Reorganizo la cola de ready segun el criterio de calendarización y si no por tiempo de llegada
      rearrange_queue(&ready_queue,P_ARRIVAL_TIME);
      temp_proc = pop_process(&ready_queue,0);
      temp_proc->state = S_ACTIVE;
      uint64_t start_time = time_counter;
      for (uint64_t i = 0; i < temp_proc->params[P_BURST_TIME]; i++)
      {
        feed_queue(&process_queue,&ready_queue,P_ARRIVAL_TIME);
        feed_queue(&waiting_queue,&ready_queue,P_IO_BLOCK);
        if(check_wip(temp_proc)){
          if(time_counter-start_time <= quantum){
            run(temp_proc);
          }else{
            exprop_reason = QUANTUM_EXPROP;
            break;
          }
        }else{
          exprop_reason = WLFINISH_EXPROP;
          break; //Proceso completado
        }
      }
      if(temp_proc != NULL){
        switch (exprop_reason)
        {
        case IO_EXPROP:
          temp_proc->state = S_IOBLOCK;
          temp_proc->params[P_WAIT_TIME] = time_counter;
          push_process(&waiting_queue,temp_proc);
          break;
        case QUANTUM_EXPROP:
          temp_proc->state = S_READY;
          temp_proc->params[P_ARRIVAL_TIME] = time_counter;
          push_process(&ready_queue,temp_proc);
          break;
        case WLFINISH_EXPROP:
          temp_proc->state = S_DONE;
          if(console_log){
            printf("PID:%i Terminado.\n",temp_proc->pid);
          }
          break;
        default:
          break;
        }
      }
    }   
  }
  Calculando = FALSE;
  printf("Tiempo:%li\n",time_counter);
  return NULL;
}

void _RR_INIT(GtkButton *button,gpointer *data){
  if(Calculando == FALSE){
    Calculando = TRUE;
    gtk_button_set_label(button,"WORKING");
    g_thread_new("thread",(GThreadFunc)_RR,data);
    g_thread_new("time",(GThreadFunc)UpdateTime,(gpointer)&time_inc);
  }
}

gpointer _FCFS(gpointer *data){
  queue_node **process_data = (queue_node **)data;
  rearrange_queue(process_data,P_ARRIVAL_TIME);
  queue_node *process_queue = copy_queue(process_data);
  rearrange_queue(&process_queue,P_ARRIVAL_TIME);

  queue_node *ready_queue = NULL;   //Cola para procesos a espera de ejecución
  queue_node *waiting_queue = NULL; //Cola para procesos ya ejecutados pero no terminados

  time_counter = 0;
  g_mutex_unlock(&time_lock);
  while (process_queue != NULL || ready_queue != NULL || waiting_queue != NULL) //Van a llegar procesos o la cola de ready aun tiene trabajo faltante o hay procesos en waiting
  {
    //Operaciones de actualizacion de colas
    feed_queue(&process_queue,&ready_queue,P_ARRIVAL_TIME);
    feed_queue(&waiting_queue,&ready_queue,P_IO_BLOCK);

    process *temp_proc = NULL;
    uint8_t exprop_reason = IO_EXPROP;

    if(ready_queue != NULL) //Hay algo en la cola de ready, ejecuto el primer proceso de la cola
    {
      //Reorganizo la cola de ready segun el criterio de calendarización y si no por tiempo de llegada
      rearrange_queue(&ready_queue,P_ARRIVAL_TIME);

      temp_proc = pop_process(&ready_queue,0);
      temp_proc->state = S_ACTIVE;
      for (uint64_t i = 0; i < temp_proc->params[P_BURST_TIME]; i++)
      {
        feed_queue(&process_queue,&ready_queue,P_ARRIVAL_TIME);
        feed_queue(&waiting_queue,&ready_queue,P_IO_BLOCK);
        if(check_wip(temp_proc)){
          //El proceso no ha terminado todo su trabajo
          run(temp_proc);
        }else{
          exprop_reason = WLFINISH_EXPROP;
          break; //Proceso completado
        }
      }
    }
    if(temp_proc != NULL){
      switch (exprop_reason)
      {
      case IO_EXPROP:
        temp_proc->state = S_IOBLOCK;
        temp_proc->params[P_WAIT_TIME] = time_counter;
        push_process(&waiting_queue,temp_proc);
        break;
      case WLFINISH_EXPROP:
        temp_proc->state = S_DONE;
        if(console_log){
            printf("PID:%i Terminado.\n",temp_proc->pid);
          }
        break;
      default:
        break;
      }
    }   
  }
  Calculando = FALSE;
  printf("Tiempo:%li\n",time_counter);
  return NULL;
}

void _FCFS_INIT(GtkButton *button,gpointer *data){
  if(Calculando == FALSE){
    Calculando = TRUE;
    gtk_button_set_label(button,"WORKING");
    g_thread_new("thread",(GThreadFunc)_FCFS,data);
    g_thread_new("time",(GThreadFunc)UpdateTime,(gpointer)&time_inc);
  }
}

gboolean UpdateUI(){
  gboolean all_done = TRUE;
  queue_node *temp = process_list;
  while (temp != NULL && Calculando)
  {
    gtk_progress_bar_set_fraction(GTK_PROGRESS_BAR(temp->proc->progressBar),(double)temp->proc->params[P_WORK_DONE]/temp->proc->params[P_WORK_LOAD]);
    char *pival_text = ftoa(temp->proc->pi_val);
    gtk_label_set_label(GTK_LABEL(temp->proc->pi_label),pival_text);
    free(pival_text);
    char *label_text = (char*)malloc(32);
    switch (temp->proc->state)
    {
    case S_ARRIVAL:
      all_done = FALSE;
      sprintf(label_text,"PID:%i[AR]",temp->proc->pid);
      break;
    case S_READY:
      all_done = FALSE;
      sprintf(label_text,"PID:%i[RE]",temp->proc->pid);
      break;
    case S_ACTIVE:
      all_done = FALSE;
      sprintf(label_text,"PID:%i[ACTIVE]",temp->proc->pid);
      break;
    case S_IOBLOCK:
      all_done = FALSE;
      sprintf(label_text,"PID:%i[IO]",temp->proc->pid);
      break;
    case S_DONE:
      sprintf(label_text,"PID:%i[DO]",temp->proc->pid);
      break;
    default:
      break;
    }
    gtk_label_set_text(GTK_LABEL(temp->proc->proc_label),label_text);
    free(label_text);
    temp = temp->next;
  }
  if(all_done){
    gtk_button_set_label(GTK_BUTTON(start_button),"DONE");
  }
  return TRUE;
}
//Load the style.css file and sets the height and color of the progress bar's
//Taken and adapted from https://stackoverflow.com/questions/48097764/gtkprogressbar-with-css-for-progress-colour-not-functioning @Jase
void set_css (void) {
    GtkCssProvider *css_provider;
    GdkDisplay *display;
    GdkScreen *screen;
    const char *css_file = "style.css";
    GError *error = NULL;

    css_provider = gtk_css_provider_new ();
    display = gdk_display_get_default ();
    screen = gdk_display_get_default_screen (display);
    gtk_style_context_add_provider_for_screen (screen, GTK_STYLE_PROVIDER (css_provider), GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
    gtk_css_provider_load_from_file(css_provider, g_file_new_for_path (css_file), &error);
    g_object_unref (css_provider);  
}


int main(int argc, char *argv[])
{
  GtkWidget *window,*grid,*algorithm_label;
  gtk_init(&argc,&argv);
  set_css();
  config_parser(argv[1]);
  window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title(GTK_WINDOW(window),"The Scheduler");
  g_signal_connect(window,"destroy",G_CALLBACK(gtk_main_quit),NULL);

  grid = gtk_grid_new();
  gtk_grid_set_row_homogeneous(GTK_GRID(grid),TRUE);
  gtk_grid_set_column_homogeneous(GTK_GRID(grid),TRUE);
  gtk_widget_set_margin_bottom(grid,10);

  gtk_container_add(GTK_CONTAINER(window),grid);

  algorithm_label = gtk_label_new("");
  gtk_grid_attach(GTK_GRID(grid),algorithm_label,0,0,3,1);

  queue_node *temp = process_list;
  int i = 0;
  while (temp != NULL)
  {
    gtk_widget_set_halign(temp->proc->progressBar,GTK_ALIGN_FILL);
    gtk_widget_set_valign(temp->proc->progressBar,GTK_ALIGN_CENTER);
    gtk_widget_set_halign(temp->proc->proc_label,GTK_ALIGN_START);
    gtk_widget_set_margin_start(temp->proc->proc_label,10);
    gtk_grid_attach(GTK_GRID(grid),temp->proc->proc_label,0,i+1,1,1);
    gtk_grid_attach(GTK_GRID(grid),temp->proc->progressBar,1,i+1,1,1);
    gtk_grid_attach(GTK_GRID(grid),temp->proc->pi_label,2,i+1,1,1);
    i++;
    temp = temp->next;
  }
  start_button = gtk_button_new_with_label("START");
  gtk_grid_attach(GTK_GRID(grid),start_button,1,process_count+2,1,1);

  switch (algorithm)
  {
  case FCFS:
    gtk_label_set_text(GTK_LABEL(algorithm_label),"First Come First Served");
    g_signal_connect(start_button,"clicked",G_CALLBACK(_FCFS_INIT),&process_list);
    break;
  case SJF:
    gtk_label_set_text(GTK_LABEL(algorithm_label),"Shortest job first (SJF)");
    g_signal_connect(start_button,"clicked",G_CALLBACK(_SJF_INIT),&process_list);
    break;
  case RR:
    gtk_label_set_text(GTK_LABEL(algorithm_label),"Round Robin (RR)");
    g_signal_connect(start_button,"clicked",G_CALLBACK(_RR_INIT),&process_list);
    break;
  case PS:
    gtk_label_set_text(GTK_LABEL(algorithm_label),"Priority Scheduling (PS)");
    g_signal_connect(start_button,"clicked",G_CALLBACK(_PS_INIT),&process_list);
    break;
  case PS_RR:
    gtk_label_set_text(GTK_LABEL(algorithm_label),"Priority Scheduling + Round Robin (PS_RR)");
    g_signal_connect(start_button,"clicked",G_CALLBACK(_PS_RR_INIT),&process_list);
    break;
  case MQS:
    gtk_label_set_text(GTK_LABEL(algorithm_label),"Multilevel Queue Scheduling (MQS)");
    g_signal_connect(start_button,"clicked",G_CALLBACK(_MQS_INIT),&process_list);
    break;
  case MFQS:
    gtk_label_set_text(GTK_LABEL(algorithm_label),"Multilevel Feedback Queue Scheduling (MFQS)");
    g_signal_connect(start_button,"clicked",G_CALLBACK(_MFQS_INIT),&process_list);
    break;
  default:
    break;
  }
  g_mutex_lock(&time_lock);
  g_timeout_add(16,(GSourceFunc)UpdateUI,NULL);
  gtk_widget_show_all(window);
  gtk_main();
  return 0;
}
