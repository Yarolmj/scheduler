build:
	gcc -o3 `pkg-config --cflags gtk+-3.0` Scheduler.c -o Scheduler `pkg-config --libs gtk+-3.0` -lm
build_debug:
	gcc -g -Wall `pkg-config --cflags gtk+-3.0` Scheduler.c -o Scheduler `pkg-config --libs gtk+-3.0` -lm
run:	
	./Scheduler examples/General.txt
